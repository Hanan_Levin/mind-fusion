import socket
from collections import deque
from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import udp_client
import numpy as np
import time
from matplotlib import pyplot as plt
import scipy.signal as signal
import pickle
import argparse
from scipy import signal
from arduino_sync_manager import ArduinoComm
import threading
import heartpy
from copy import copy
from constants import SWITCH_USER_TIME

debug = True

SELF_IP = "127.0.0.1"
SECOND_PC_PORT = 7000

OUT_MUSIC_PORT = 9000

WINDOW_SIZE = 7
WINDOW_HEART = 400
WAIT_BEFORE_SEND = 0.2
WAIT_BEFORE_SEND_HEART = 0.2

ARDUINOCOM = None
MINIMAL_HEART_WINDOW_LENGTH = 300
NUM_EEG_CHANNELS = 4
ERROR_INDICATOR = 2
HEART_ERROR = 63


class MuseData():
    def __init__(self, single_user, in_port, second_pc_ip, arduino_com):
        self.in_ip = socket.gethostbyname_ex(socket.gethostname())[2][-1]
        self.in_port = in_port
        self.second_pc_ip = second_pc_ip
        self.arduino_com = arduino_com
        self.two_users = not single_user
        self.started = None
        self.exp_start_time = None
        self.in_heart_thread_1 = []
        self.in_heart_thread_2 = []
        self.in_eeg_thread = []
        self.heart_1_per_second = 0
        self.heart_2_per_second = 0
        self.battery_1_charge = 0
        self.battery_2_charge = 0
        self.arduino = None
        if not self.two_users and ARDUINOCOM:
            try:
                self.arduino = ArduinoComm(ARDUINOCOM)
            except Exception as e:
                raise

        self.client_music = udp_client.SimpleUDPClient(SELF_IP, OUT_MUSIC_PORT)
        if self.two_users:
            self.client_pc = udp_client.SimpleUDPClient(second_pc_ip, SECOND_PC_PORT)
            try:
                self.arduino = ArduinoComm(arduino_com)
            except Exception as e:
                raise

        # data windows user 1
        self.alpha_window_1, self.beta_window_1, self.gamma_window_1, self.delta_window_1 = \
            [deque(maxlen=WINDOW_SIZE * NUM_EEG_CHANNELS) for _ in range(4)]
        self.heart_window_1 = deque(maxlen=WINDOW_HEART)

        # data windows user 2
        self.alpha_window_2, self.beta_window_2, self.gamma_window_2, self.delta_window_2 = \
            [deque(maxlen=WINDOW_SIZE * NUM_EEG_CHANNELS) for _ in range(4)]
        self.heart_window_2 = deque(maxlen=WINDOW_HEART)

        self.heart_averages_window_1 = deque(maxlen=6)
        self.heart_averages_window_2 = deque(maxlen=6)

        # times
        start_time = time.time()
        self.alpha_1_previous, self.beta_1_previous, self.gamma_1_previous, self.delta_1_previous, \
        self.heart_1_previous, self.alpha_2_previous, self.beta_2_previous, self.gamma_2_previous, \
        self.delta_2_previous, self.heart_2_previous = [IntMemberReference(start_time) for _ in range(10)]

        # averages
        self.alpha_1_average, self.beta_1_average, self.gamma_1_average, self.delta_1_average, \
        self.heart_1_average, self.alpha_2_average, self.beta_2_average, self.gamma_2_average, \
        self.delta_2_average, self.heart_2_average, self.intensity_1_average, self.intensity_2_average = \
            [IntMemberReference(0) for _ in range(12)]

        self.dispatcher = dispatcher.Dispatcher()

        self.dispatcher.map("/muse1/elements/alpha_relative", self.eeg_signal_handler, self.alpha_window_1, 'alpha_1',
                            self.alpha_1_previous, self.alpha_1_average)
        self.dispatcher.map("/muse1/elements/beta_relative", self.eeg_signal_handler, self.beta_window_1, 'beta_1',
                            self.beta_1_previous, self.beta_1_average)
        self.dispatcher.map("/muse1/elements/gamma_relative", self.eeg_signal_handler, self.gamma_window_1, 'gamma_1',
                            self.gamma_1_previous, self.gamma_1_average)
        self.dispatcher.map("/muse1/elements/delta_relative", self.eeg_signal_handler, self.delta_window_1, 'delta_1',
                            self.delta_1_previous, self.delta_1_average)

        self.dispatcher.map("/muse1/ppg", self.heart_beat_handler, self.heart_window_1, 'heart_1',
                            self.heart_1_previous, self.heart_1_average)

        self.dispatcher.map("/muse2/elements/alpha_relative", self.eeg_signal_handler, self.alpha_window_2, 'alpha_2',
                            self.alpha_2_previous, self.alpha_2_average)
        self.dispatcher.map("/muse2/elements/beta_relative", self.eeg_signal_handler, self.beta_window_2, 'beta_2',
                            self.beta_2_previous, self.beta_2_average)
        self.dispatcher.map("/muse2/elements/gamma_relative", self.eeg_signal_handler, self.gamma_window_2, 'gamma_2',
                            self.gamma_2_previous, self.gamma_2_average)
        self.dispatcher.map("/muse2/elements/delta_relative", self.eeg_signal_handler, self.delta_window_2, 'delta_2',
                            self.delta_2_previous, self.delta_2_average)
        self.dispatcher.map("/muse2/ppg", self.heart_beat_handler, self.heart_window_2, 'heart_2',
                            self.heart_2_previous, self.heart_2_average)

        # battery
        self.dispatcher.map("/muse1/batt", self.battery_handler, 1)
        self.dispatcher.map("/muse2/batt", self.battery_handler, 2)

        self.server = osc_server.ThreadingOSCUDPServer((self.in_ip, self.in_port), self.dispatcher)
        server_thread = threading.Thread(target=self.server.serve_forever)
        server_thread.daemon = True
        server_thread.start()

    def close(self):
        if self.arduino and self.arduino.is_connected:
            self.arduino.send_int_data(0)  # kill the light
            self.arduino.close()
        self.server.shutdown()
        self.server.server_close()

    def eeg_signal_handler(self, unused_addr, args, ch1, ch2, ch3, ch4):
        signal_window = args[0]
        signal_name = args[1]
        signal_previous_time_reference = args[2]
        signal_previous_time = signal_previous_time_reference.get_int_member()
        signal_average_reference = args[3]
        signal_window.extend([ch1, ch2, ch3, ch4])
        user = 1 if '1' in signal_name else 2
        cur_time = time.time()
        elapsed_time = cur_time - signal_previous_time
        average_value = None
        self.in_eeg_thread.append(0)
        if elapsed_time > WAIT_BEFORE_SEND and len(self.in_eeg_thread) == 1:
            window = np.array(signal_window)
            osc_name = f"/{signal_name.split('_')[0]}"
            if len(window) == 0:
                average_value = ERROR_INDICATOR
            else:
                non_zero_entries = window[np.where(window > 0)]
                if len(non_zero_entries) > 0:
                    mean_non_zero_in_window = np.mean(non_zero_entries)
                    average_value = mean_non_zero_in_window
                else:
                    average_value = ERROR_INDICATOR
            self.send_osc_to_generator(user, osc_name, average_value)
            signal_previous_time_reference.set_int_member(cur_time)
            signal_average_reference.set_int_member(average_value)
            self.in_eeg_thread.pop(0)
        else:
            self.in_eeg_thread.pop(0)
        if self.two_users and self.arduino.is_connected and self.started:
            self.send_data_to_arduino()  # todo make sure that this is a good place for this.
        if not self.two_users and ARDUINOCOM:
            if self.alpha_1_average.get_int_member() < 0.18:
                self.arduino.send_int_data(254)
            elif self.alpha_1_average.get_int_member() < 0.3:
                self.arduino.send_int_data(100)
            else:
                self.arduino.send_int_data(0)

        self.update_intensity(user)

    def heart_beat_handler(self, unused_addr, args, ppg0, ppg1, ppg2):

        heart_window = args[0]
        signal_name = args[1]
        signal_previous_time_reference = args[2]
        signal_previous_time = signal_previous_time_reference.get_int_member()
        signal_average_reference = args[3]
        user = 1 if '1' in signal_name else 2
        amplitude = ppg1
        cur_time = time.time()

        heart_window.append((amplitude, cur_time))
        elapsed_time = cur_time - signal_previous_time
        average_value = None
        if user == 1:
            self.in_heart_thread_1.append(0)
            in_thread = self.in_heart_thread_1
        else:
            self.in_heart_thread_2.append(0)
            in_thread = self.in_heart_thread_2

        if elapsed_time > WAIT_BEFORE_SEND_HEART and len(heart_window) > MINIMAL_HEART_WINDOW_LENGTH and \
                len(in_thread) == 1:
            # a=2
            heart_copy = copy(heart_window)
            amplitudes = [amplitude for (amplitude, time) in heart_copy]
            heart_data = np.array(amplitudes)
            # heart_data = np.array(heart_copy)

            heart_filtered = butter_highpass_filter(heart_data, 1, 60)
            # heart_data /= np.max(heart_data)
            peaks = signal.find_peaks_cwt(heart_filtered, np.arange(1, 25))
            osc_name = "/heart_rate"

            if len(peaks) == 0:
                average_value = HEART_ERROR + float(np.random.rand(1)) * 3

            else:
                window_time = heart_copy[-1][1] - heart_copy[0][1]
                if user == 1:
                    self.heart_1_per_second = len(heart_copy) / window_time
                else:
                    self.heart_2_per_second = len(heart_copy) / window_time


                heart_rate = len(peaks) * (60 / window_time)

                # todo - check below: dirty fix for bad heart rate
                if heart_rate < 63:
                    heart_rate = 63 + float((np.random.rand(1) - 0.5) * 4)  # 63 +-2
                if heart_rate > 109:
                    heart_rate = 109 + float((np.random.rand(1) - 0.5) * 4)  # 109 +-2


                if user == 1:
                    self.heart_averages_window_1.append(heart_rate)
                else:
                    self.heart_averages_window_2.append(heart_rate)
                averages_window = self.heart_averages_window_1 if user == 1 else self.heart_averages_window_2
                average_value = np.average(averages_window)


            self.send_osc_to_generator(user, osc_name, average_value)
            signal_previous_time_reference.set_int_member(cur_time)
            signal_average_reference.set_int_member(average_value)

        if user == 1:
            self.in_heart_thread_1.pop()
        else:
            self.in_heart_thread_2.pop()

    def battery_handler(self, unused_addr, args, charge_percent, _1, _2):
        user = args[0]
        if user == 1:
            self.battery_1_charge = charge_percent
        else:
            self.battery_2_charge = charge_percent

    def start_experience(self):
        self.started = True
        self.exp_start_time = time.time()

    def end_experience(self):
        if self.arduino and self.arduino.is_connected:
            self.arduino.send_int_data(0)
        self.started = False
        self.exp_start_time = None

    def send_data_to_arduino(self):
        # send all the relevant data for the syncing manager in the arduino to handle.
        self.arduino.manage_data(self.alpha_1_average.get_int_member(), self.alpha_2_average.get_int_member(),
                                 self.heart_1_average.get_int_member(), self.heart_2_average.get_int_member(),
                                 self.exp_start_time)

    def update_intensity(self, user):
        # todo improve this, and also send the data to the music generator, so that it won't calculate it by it self.

        if user == 1:
            self.intensity_1_average.set_int_member(
                max(1 - (self.alpha_1_average.get_int_member() * 2), 0))
            self.send_osc_to_generator(user, '/intensity', self.intensity_1_average.get_int_member())
        elif user == 2:  # todo change like user 1
            # print("CHANGE ME!")
            self.intensity_2_average.set_int_member(max(1 - (self.alpha_2_average.get_int_member() * 2), 0))
            self.send_osc_to_generator(user, '/intensity', self.intensity_2_average.get_int_member())

    def start_second_pc_generator(self):
        self.client_pc.send_message('/start', 0)

    def stop_second_pc_generator(self):
        self.client_pc.send_message('/stop', 0)

    def close_second_pc_generator(self):
        self.client_pc.send_message('/close', 0)

    def send_osc_to_generator(self, user, osc_name, value):
        cur_time = time.time()
        if self.two_users and self.started and cur_time - self.exp_start_time > SWITCH_USER_TIME:  # make each user hear the other.
            # print('STARTED SECOND PHASE')  # todo
            if user == 1:
                self.client_pc.send_message(osc_name, value)
            else:
                self.client_music.send_message(osc_name, value)
        else:
            if user == 1:
                self.client_music.send_message(osc_name, value)
            elif self.two_users:  # used only to block when simulating 2 muses on the pc
                self.client_pc.send_message(osc_name, value)



def get_UDP_messages(ip="127.0.0.1", port=8000):
    """
    Get the OSC message as UDP, mainly for debugging purposes.
    """
    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    sock.bind((ip, port))
    print("Start listening on ip:{ip} on port:{port}".format(ip=ip, port=[port]))
    while True:
        data, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
        print("received message:", str(data))


def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = signal.butter(order, normal_cutoff, btype='high', analog=False)
    return b, a


def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y


class IntMemberReference:
    def __init__(self, value):
        self.int_member = value

    def get_int_member(self):
        return self.int_member

    def set_int_member(self, new_val):
        self.int_member = new_val

