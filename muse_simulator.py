
import argparse
import random
import time
import numpy as np
import socket

from pythonosc import osc_message_builder
from pythonosc import udp_client

if __name__ == "__main__":
    ip = socket.gethostbyname_ex(socket.gethostname())[2][-1]
    port = 9500

    client = udp_client.SimpleUDPClient(ip, port)
    print(f"ip:{ip}, port:{port}")

    while (True):
        rate1 = np.random.randint(0, 100)
        rate2 = np.random.randint(0, 100)
        alpha_1 = float(np.random.rand(1))
        alpha_2 = float(np.random.rand(1))

        client.send_message("/muse1/ppg", [100, rate1, 100])
        client.send_message("/muse2/ppg", [100, rate2, 100])

        client.send_message("/muse1/elements/alpha_relative", [alpha_1, alpha_1, alpha_1, alpha_1])
        client.send_message("/muse2/elements/alpha_relative", [alpha_2, alpha_2, alpha_2, alpha_2])

        # print(rate1, rate2)
        time.sleep(0.02)
