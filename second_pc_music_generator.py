import time
import rtmidi
import mido
import numpy as np
import threading
from pythonosc import dispatcher
from pythonosc import osc_server
import socket
from music_generator import *

IN_PORT = 10001


class SecondMusicGenerator(MusicGenerator):
    def __init__(self):
        super().__init__(user=2, port=IN_PORT)
        # self.ip = socket.gethostbyname_ex(socket.gethostname())[2][-1]
        # self.in_port = IN_PORT

    def set_start(self, unused_addr, _):
        print("starting")
        self.start()

    def set_stop(self, unused_addr, _):
        print("stopping")
        self.stop()

    def set_close(self, unused_addr, _):
        print("closing - terminating client")
        self.close()
        exit(0)


    def set_dispatcher(self):
        # Set up an osc server to receive the processed data from the muse.
        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map("/heart_rate", self.set_bpm)
        self.dispatcher.map("/alpha", self.set_alpha)
        self.dispatcher.map("/beta", self.set_beta)
        self.dispatcher.map("/gamma", self.set_gamma)
        self.dispatcher.map("/delta", self.set_delta)
        self.dispatcher.map("/blink", self.blink)  # todo? tried it out on the muselab but seemed un reliable.

        # communication  #todo maybe in a seperate function
        self.dispatcher.map("/start", self.set_start)  # todo maybe check that it hasn't already started
        self.dispatcher.map("/stop", self.set_stop)
        self.dispatcher.map("/close", self.set_close)



if __name__ == '__main__':
    generator = SecondMusicGenerator()
    print("Generator initialized")
    while True:
        time.sleep(5)