from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.uix.popup import Popup
import socket
import pickle
import os.path
from kivy.clock import Clock
from muse_data_handler import MuseData
from music_generator import MusicGenerator


class StartWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)  
        self.single = True

    def single_dual_btn(self, instace):
        monitor_screen = self.manager.get_screen('monitor')
        setup_screen = self.manager.get_screen('setup')
        if 'Single' in instace.text:
            self.single = True
        else:
            self.single = False
        setup_screen.update_users(single=self.single)
        monitor_screen.update_users(single=self.single)

        app.root.current = 'setup'
        self.manager.transition.direction = 'left'


class SetUpWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ip = socket.gethostbyname_ex(socket.gethostname())[2][-1]

    def update_users(self, single=True):
        if single:
            fields_to_nullify = [self.ids['label_second_pc_ip'], self.ids['second_pc_ip'],
                                 self.ids['label_arduino_com'],
                                 self.ids['arduino_com']]
            for field in fields_to_nullify:
                field.size_hint = (0, 0)
                field.text = ""
            if os.path.isfile('single_setup_fields.pickle'):
                with open('single_setup_fields.pickle', 'rb') as f:
                    fields_dict = pickle.load(f)
                self.ids['iphone_ip'].text = fields_dict['iphone_ip']
                self.ids['in_port'].text = fields_dict['in_port']


        else:
            self.ids['label_second_pc_ip'].text = "Second pc [b]IP[/b]:"
            self.ids['label_second_pc_ip'].size_hint = (1, 1)
            self.ids['second_pc_ip'].size_hint = (1, 1)
            self.ids['label_arduino_com'].text = 'Arduino [b]COM[/b]:'
            self.ids['label_arduino_com'].size_hint = (1, 1)
            self.ids['arduino_com'].size_hint = (1, 1)
            if os.path.isfile('dual_setup_fields.pickle'):
                with open('dual_setup_fields.pickle', 'rb') as f:
                    fields_dict = pickle.load(f)
                self.ids['iphone_ip'].text = fields_dict['iphone_ip']
                self.ids['in_port'].text = fields_dict['in_port']
                self.ids['second_pc_ip'].text = fields_dict['second_pc_ip']
                self.ids['arduino_com'].text = fields_dict['arduino_com']

    def connect(self):
        self.start_screen = self.manager.get_screen('start')
        self.monitor_screen = self.manager.get_screen('monitor')
        if self.start_screen.single == True:
            iphone_ip = self.ids['iphone_ip'].text
            in_port = self.ids['in_port'].text

            if not iphone_ip or not in_port:
                popup = Popup(title='Error', title_color=[1, 0, 0, 1], title_size='20sp',
                              content=Label(text='Fill in both IP and PORT before connecting',
                                            color=[1, 1, 0, 1]), size_hint=(0.4, 0.4))
                popup.open()
                return
            fields_dict = {'iphone_ip': iphone_ip, 'in_port': in_port}
            with open('single_setup_fields.pickle', 'wb') as f:
                pickle.dump(fields_dict, f)
        else:
            fields = [self.ids['iphone_ip'].text, self.ids['in_port'].text, self.ids['second_pc_ip'].text,
                      self.ids['arduino_com'].text]
            if "" in fields:
                popup = Popup(title='Error', title_color=[1, 0, 0, 1], title_size='20sp',
                              content=Label(text='Fill in all fields before connecting',
                                            color=[1, 1, 0, 1]), size_hint=(0.4, 0.4))
                popup.open()
                return
            fields_dict = {'iphone_ip': self.ids['iphone_ip'].text, 'in_port': self.ids['in_port'].text,
                           'second_pc_ip': self.ids['second_pc_ip'].text, 'arduino_com': self.ids['arduino_com'].text}
            with open('dual_setup_fields.pickle', 'wb') as f:
                pickle.dump(fields_dict, f)

        self.monitor_screen.iphone_ip, self.monitor_screen.in_port = self.ids['iphone_ip'].text, self.ids[
            'in_port'].text
        self.monitor_screen.second_pc_ip, self.monitor_screen.arduino_com = self.ids['second_pc_ip'].text, self.ids[
            'arduino_com'].text
        self.monitor_screen.update_connection_details()
        try:
            self.initiate_classes()
        except Exception as e:
            popup = Popup(title='Error', title_color=[1, 0, 0, 1], title_size='20sp',
                          content=Label(
                              text='Problem initiating connection.\nMost likely an incorrect arduino '
                                   'port or you have have not closed another application of the GUI. the error is:\n\n'
                                   + str(e),
                              color=[1, 1, 0, 1]), size_hint=(0.4, 0.4))
            popup.open()
            return
        app.root.current = 'monitor'
        self.manager.transition.direction = 'left'

    def initiate_classes(self):
        try:
            self.monitor_screen.muse_data = MuseData(self.start_screen.single, int(self.monitor_screen.in_port),
                                                     self.monitor_screen.second_pc_ip, self.monitor_screen.arduino_com)
        except Exception as e:
            raise
        self.monitor_screen.music_generator = MusicGenerator()


class SetUpWindowDual(Screen):
    pass


class MonitorWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.iphone_ip = ""
        self.in_port = ""
        self.second_pc_ip = ""
        self.aruidno_com = ""
        self.timer = 0
        self.timer_event = None
        self.sliders_update_event = None
        self.running = False
        self.music_generator = None
        self.muse_data = None
        self.takeover_heart_1, self.takeover_alpha_1, self.takeover_beta_1, self.takeover_gamma_1, self.takeover_delta_1, \
        self.takeover_heart_2, self.takeover_beta_2, self.takeover_gamma_2, self.takeover_delta_2, self.takeover_alpha_2, \
        self.takeover_intensity_1, self.takeover_intensity_2 = \
            [False] * 12

    def update_users(self, single=True):
        start_screen = self.manager.get_screen('start')
        user_2_fields = [self.ids['user_seperator'], self.ids['heart_2_grid'], self.ids['alpha_2_grid'],
                         self.ids['beta_2_grid'], self.ids['gamma_2_grid'], self.ids['delta_2_grid'],
                         self.ids['intensity_2_grid'], self.ids['user_1_text'], self.ids['user_2_text']]
        if start_screen.single:
            for field in user_2_fields:
                field.size_hint_x = 0
            self.ids['user_2_text'].text = ""
            self.ids['user_1_text'].text = ""

        else:
            for field in user_2_fields:
                field.size_hint_x = 1
            self.ids['user_1_text'].text = "User 1"
            self.ids['user_2_text'].text = "User 2"

    def update_connection_details(self):
        start_screen = self.manager.get_screen('start')
        label = self.ids['ip_and_port']
        label.text = f"Listening for muse data on [i][b]IP: {self.iphone_ip}, PORT: {self.in_port}[/b][/i]"
        label.markup = True
        label = self.ids['second_ip_and_com']
        if not start_screen.single:
            label.text = f"Sending to second pc on [i][b]IP: {self.second_pc_ip}. arduino COM: {self.arduino_com}[/b][/i]"
        else:
            label.text = ""
        label.markup = True

    def take_over_btn(self, instance):
        turn_on = instance.text == "Take\n over:\n Off"
        self.set_take_over(instance.info, turn_on)
        if turn_on:
            instance.background_color = [1, 0, 0, 1]
            instance.text = "Take\n over:\n On"
        else:
            instance.background_color = [0, 1, 1, 1]
            instance.text = "Take\n over:\n Off"

    def set_take_over(self, info, turn_on):
        if "alpha_1" in info:
            self.takeover_alpha_1 = True if turn_on else False
            self.music_generator.toggle_take_over_alpha()
        elif "beta_1" in info:
            self.takeover_beta_1 = True if turn_on else False
            self.music_generator.toggle_take_over_beta()
        elif "gamma_1" in info:
            self.takeover_gamma_1 = True if turn_on else False
            self.music_generator.toggle_take_over_gamma()
        elif "delta_1" in info:
            self.takeover_delta_1 = True if turn_on else False
            self.music_generator.toggle_take_over_delta()
        elif "heart_1" in info:
            self.takeover_heart_1 = True if turn_on else False
            self.music_generator.toggle_take_over_bpm()
        elif "alpha_2" in info:
            self.takeover_beta_2 = True if turn_on else False
        elif "beta_2" in info:
            self.takeover_beta_2 = True if turn_on else False
        elif "gamma_2" in info:
            self.takeover_gamma_2 = True if turn_on else False
        elif "delta_2" in info:
            self.takeover_delta_2 = True if turn_on else False
        elif "heart_2" in info:
            self.takeover_heart_2 = True if turn_on else False
        elif "intensity_1" in info:
            self.takeover_intensity_1 = True if turn_on else False
            self.music_generator.toggle_take_over_intensity()
        elif "intensity_2" in info:
            self.takeover_intensity_2 = True if turn_on else False

    def sliders_update(self, _):  # todo this code stops working when I switch users...
        if not self.takeover_alpha_1:
            self.ids['alpha_1_slider'].value = float(self.muse_data.alpha_1_average.get_int_member())
        else:
            self.music_generator.slider_alpha(self.ids['alpha_1_slider'].value)
        if not self.takeover_beta_1:
            self.ids['beta_1_slider'].value = float(self.muse_data.beta_1_average.get_int_member())
        else:
            self.music_generator.slider_beta(self.ids['beta_1_slider'].value)
        if not self.takeover_gamma_1:
            self.ids['gamma_1_slider'].value = float(self.muse_data.gamma_1_average.get_int_member())
        else:
            self.music_generator.slider_gamma(self.ids['gamma_1_slider'].value)
        if not self.takeover_delta_1:
            self.ids['delta_1_slider'].value = float(self.muse_data.delta_1_average.get_int_member())
        else:
            self.music_generator.slider_delta(self.ids['delta_1_slider'].value)
        if not self.takeover_heart_1:
            self.ids['heart_1_slider'].value = float(self.muse_data.heart_1_average.get_int_member())
        else:
            self.music_generator.slider_heart(self.ids['heart_1_slider'].value)

        if not self.takeover_intensity_1:
            self.ids['intensity_1_slider'].value = float(self.muse_data.intensity_1_average.get_int_member())
        else:
            self.music_generator.slider_intensity(self.ids['intensity_1_slider'].value)
        # todo take over not supported for user 2

        if not self.takeover_alpha_2:
            self.ids['alpha_2_slider'].value = float(self.muse_data.alpha_2_average.get_int_member())
        if not self.takeover_beta_2:
            self.ids['beta_2_slider'].value = float(self.muse_data.beta_2_average.get_int_member())
        if not self.takeover_gamma_2:
            self.ids['gamma_2_slider'].value = float(self.muse_data.gamma_2_average.get_int_member())
        if not self.takeover_delta_2:
            self.ids['delta_2_slider'].value = float(self.muse_data.delta_2_average.get_int_member())
        if not self.takeover_heart_2:
            self.ids['heart_2_slider'].value = float(self.muse_data.heart_2_average.get_int_member())

        if not self.takeover_intensity_2:
            self.ids['intensity_2_slider'].value = float(self.muse_data.intensity_2_average.get_int_member())
        self.ids['user_1_text'].size_hint_y = 2

        battery_1 = self.muse_data.battery_1_charge
        if battery_1:
            self.ids['user_1_text'].text = f"[b]User 1[/b]  Charge: {int(battery_1)}% \n " \
                                       f"{int(self.muse_data.heart_1_per_second)} heart messages per second"
        else:
            self.ids['user_1_text'].text = f"[b]User 1[/b]"
            # print("User 1: VERY LOW BATTERY!")
        start_screen = self.manager.get_screen('start')

        if not start_screen.single:
            battery_2 = self.muse_data.battery_2_charge
            self.ids['user_2_text'].size_hint_y = 2
            if battery_2:
                self.ids['user_2_text'].text = f"[b]User 2[/b]  Charge: {int(battery_2)}%\n " \
                                            f"{int(self.muse_data.heart_2_per_second)} heart messages per second"
            else:
                self.ids['user_2_text'].text = f"[b]User 2[/b]"
                # print("User 2: VERY LOW BATTERY!")

        # print(self.muse_data.heart_1_average)

    def start_btn(self, instance):
        start_screen = self.manager.get_screen('start')
        if instance.text == "Start":
            self.running = True
            instance.background_color = [1, 0, 0, 1]
            instance.text = "Stop"
            self.timer_event = Clock.schedule_interval(self.timer_increase, 0.1)
            self.music_generator.start()
            self.muse_data.start_experience()
            if not start_screen.single:
                self.muse_data.start_second_pc_generator()  # todo, ugly, create seperate communication component
            self.sliders_update_event = Clock.schedule_interval(self.sliders_update, 0.1)
        else:
            self.running = False
            instance.background_color = [0, 1, 0, 1]
            instance.text = "Start"
            Clock.unschedule(self.timer_event)
            Clock.unschedule(self.sliders_update_event)
            self.timer = 0
            self.ids['timer'].text = f"[b]Timer[/b]"
            self.music_generator.stop()
            self.muse_data.end_experience()
            if not start_screen.single:
                self.muse_data.stop_second_pc_generator()  # todo, ugly, create seperate communication component

    def timer_increase(self, _):  # todo check second argument. might be useful
        self.timer += 0.1
        self.ids['timer'].text = "Time since started: {0:.1f}".format(self.timer)

    def back_btn(self):
        start_screen = self.manager.get_screen('start')
        if self.running:
            Clock.unschedule(self.timer_event)
            self.timer = 0
            self.ids['timer'].text = f"[b]Timer[/b]"
        app.root.current = 'setup'
        self.manager.transition.direction = 'right'
        self.music_generator.close()
        if not start_screen.single:
            self.muse_data.close_second_pc_generator()  # todo this might not arrive, especially that you close the server after..? or do you?
        self.muse_data.close()
        start_button = self.ids['start_btn']
        start_button.background_color = 0, 1, 0, 1
        start_button.text = "Start"

    # def on_request_close(self):  # todo probably irrelavant. delete
    #     self.music_generator.close()


class WindowManager(ScreenManager):
    pass


class LabelColor(Label):
    pass


class ButtonInfo(Button):
    pass


class MindsicianApp(App):
    def build(self):
        return kv

    def on_stop(self):
        cur_screen = self.root.current_screen.name
        if cur_screen == 'monitor':
            monitor_screen = self.root.current_screen.manager.get_screen('monitor')
            monitor_screen.back_btn()



if __name__ == '__main__':
    kv = Builder.load_file("mindsician.kv")

    app = MindsicianApp()
    app.run()
