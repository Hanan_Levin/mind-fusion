#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        6 // On Trinket or Gemma, suggest changing this to 1

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 4 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 30 // Time (in milliseconds) to pause between pixels
int power = 0;

void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  Serial.begin(9600);
}

void loop() {
  //pixels.clear(); // Set all pixel colors to 'off'
  if (Serial.available()>0)
   {
    power = Serial.read();
    if (power == 2)  // face on face
    {
      for(int i=0; i<NUMPIXELS; i++){
        pixels.setPixelColor(i, pixels.Color(0, 255, 0));
        pixels.setBrightness(255);
        pixels.show();
      }
    }
    else if (power == 1)  // 2 face
    {
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(0, 255, 0));
      pixels.setBrightness(255);
      pixels.show();
      pixels.setPixelColor(2, pixels.Color(0, 255, 0));
      pixels.setBrightness(255);
      pixels.show();
    }
    else if (power == 0)
    {
     pixels.clear(); 
     pixels.show();
    }
    else
    {
      pixels.setPixelColor(0, pixels.Color(0, 255, 0));
      pixels.setBrightness(power);
      pixels.show();
      pixels.setPixelColor(2, pixels.Color(0, 255, 0));
      pixels.setBrightness(power);
      pixels.show();
    }
   }
}
