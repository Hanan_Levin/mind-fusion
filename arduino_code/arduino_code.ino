int  lightpin1= 9; // select the pin for the red LED
int  lightpin2= 11; // select the pin for the red LED
int power = 0;

void setup () {
  pinMode (lightpin1, OUTPUT);
  pinMode (lightpin2, OUTPUT);

  Serial.begin(9600);
}

void loop () {
   if (Serial.available()>0)
   {
    power = Serial.read();
    if (power == 2)
    {
      analogWrite(lightpin1,  254);
      analogWrite(lightpin2,  254);
    }
    else if (power == 1)
    {
      analogWrite(lightpin1,  254);
      analogWrite(lightpin2,  0);
    }
    else if (power == 0)
    {
      analogWrite(lightpin1,  0);
      analogWrite(lightpin2,  0);
    }
    else
    {
      analogWrite(lightpin1,  power);
      analogWrite(lightpin2,  power);
    }
   }
}
