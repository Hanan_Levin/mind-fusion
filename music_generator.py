import time
import rtmidi
import mido
import numpy as np
import threading
from pythonosc import dispatcher
from pythonosc import osc_server
from constants import INTRODUCNTION_SCENE_END, SETUP_1_END, HEAR_SELF_END, SETUP_2_END, HEAR_OTHER_END

SELF_IP = "127.0.0.1"
IN_PORT = 9000

MINUTE = 60

SCALE_C_MINOR = [0, 2, 3, 5, 7, 8, 10]  # c minor
SCALE_C_MAJOR = [0, 2, 4, 5, 7, 8, 11]  # c minor

SCALE_G_MAJOR = [0, 2, 4, 6, 7, 9, 11]  # g major
SCALE_F_MAJOR = [0, 2, 4, 5, 7, 9, 10]
SCALE_F_MINOR = [0, 2, 3, 5, 7, 8, 10]


OCTAVE_INTERVAL = 12
BAR_UNIT = 1 / 16  # todo - maybe this value should change during run time
NO_ACTION = "NA"
SIXTEENTH = 1
EIGHTH = 2
QUARTER = 4
HALF = 8
FULL = 16
BEAT_TO_UNIT_RATIO = 1 / QUARTER

BEAT_MSG = mido.Message('note_on', note=40, velocity=120, time=QUARTER)
BEAT_OFF_MSG = mido.Message('note_off', note=40, velocity=120)

START_MSG = mido.Message('note_on', note=1, channel=0)
STOP_MSG = mido.Message('note_on', note=2, channel=0)


## conttrol traks: channel 0: volume for instruments
## channel 1: volume for first sound effect (rain)
## channel 2: volume for second sound effect (birds)
## channel 3: spatial spread for instruments

class MusicGenerator():
    def __init__(self, user=1, port=IN_PORT):
        """ user can be 1 or 2"""
        self.user = user
        self.ip = SELF_IP
        self.in_port = port
        self.set_tracks()
        self.tracks = [self.beat_track, self.pad_track, self.melody_track, self.counter_melody_track, self.bass_track,
                       self.choir_track]
        self.bpm, self.alpha, self.beta, self.gamma, self.delta, self.intensity = 85, 0.4, 0.5, 0.5, 0.5, 0.5
        self.seconds_per_beat = MINUTE / self.bpm
        self.beats_per_bar = 4
        self.start_time = None
        self.cur_notes = []
        self.set_dispatcher()
        self.scene = None
        self.generator_stop_event = None
        # run the server on a different thread
        self.listen()

        self.started_setup_1 = False
        self.started_hear_self = False
        self.started_setup_2 = False
        self.started_hear_other = False
        self.finished_all = False

        self.scale = SCALE_C_MINOR

        self.take_over_alpha = False
        self.take_over_beta = False
        self.take_over_gamma = False
        self.take_over_delta = False
        self.take_over_heart = False
        self.take_over_intensity = False

        self.stop()  # used to make sure the recording is in its start

    def set_dispatcher(self):
        # Set up an osc server to receive the processed data from the muse.
        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map("/heart_rate", self.set_bpm)
        self.dispatcher.map("/alpha", self.set_alpha)
        self.dispatcher.map("/beta", self.set_beta)
        self.dispatcher.map("/gamma", self.set_gamma)
        self.dispatcher.map("/delta", self.set_delta)
        self.dispatcher.map("/intensity", self.set_intensity)

        self.dispatcher.map("/blink", self.blink)  # todo? tried it out on the muselab but seemed un reliable.

    def set_tracks(self):
        # available_ports = mido.get_output_names()
        if self.user == 1:
            self.control_track = mido.open_output('01. Internal MIDI 1', autoreset=True)
            self.beat_track = mido.open_output('02. Internal MIDI 2', autoreset=True)
            self.pad_track = mido.open_output('03. Internal MIDI 3', autoreset=True)
            self.melody_track = mido.open_output('04. Internal MIDI 4', autoreset=True)
            self.counter_melody_track = mido.open_output('05. Internal MIDI 5', autoreset=True)
            self.bass_track = mido.open_output('06. Internal MIDI 6', autoreset=True)
            self.choir_track = mido.open_output('07. Internal MIDI 7', autoreset=True)
        elif self.user == 2:
            self.control_track = mido.open_output('08. Internal MIDI 8', autoreset=True)
            self.beat_track = mido.open_output('09. Internal MIDI 9', autoreset=True)
            self.pad_track = mido.open_output('10. Internal MIDI 10', autoreset=True)
            self.melody_track = mido.open_output('11. Internal MIDI 11', autoreset=True)
            self.counter_melody_track = mido.open_output('12. Internal MIDI 12', autoreset=True)
            self.bass_track = mido.open_output('13. Internal MIDI 13', autoreset=True)
            self.choir_track = mido.open_output('14. Internal MIDI 14', autoreset=True)

    def listen(self):
        self.server = osc_server.ThreadingOSCUDPServer((self.ip, self.in_port), self.dispatcher)
        server_thread = threading.Thread(target=self.server.serve_forever)
        server_thread.daemon = True
        server_thread.start()

    def turn_notes_off(self):
        cur_time = time.time()
        removed_notes = []
        for i, note in enumerate(self.cur_notes):
            track, note, velocity, start, length = note
            if cur_time - start > length:
                msg = mido.Message('note_off', note=note, velocity=velocity)
                self.tracks[track].send(msg)
                removed_notes.append(i)
        for i in removed_notes[::-1]:
            self.cur_notes.remove(self.cur_notes[i])

    def compose_beat_bar(self, beats_per_bar):
        beat_bar = []
        for i in range(int(beats_per_bar * QUARTER)):
            if i % QUARTER == 0:
                beat_bar.append(BEAT_MSG)
            else:
                beat_bar.append(NO_ACTION)
        return beat_bar

    def compose_bass_bar(self, beats_per_bar):

        if self.intensity < 0.4:
            bass_time = FULL
        elif 0.3 <= self.intensity < 0.7:
            bass_time = HALF
        else:
            bass_time = QUARTER
        # bass_time = FULL

        bass_bar = []
        cur_time = time.time()

        if self.start_time and cur_time - self.start_time < 5:
            for i in range(int(beats_per_bar * QUARTER)):
                bass_bar.append(NO_ACTION)
            return bass_bar
        for i in range(int(beats_per_bar * QUARTER)):
            if i % bass_time == 0:
                octave = np.random.randint(3, 5)
                note = np.random.choice(self.scale) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(20, 40)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=FULL + EIGHTH)
                bass_bar.append(msg)
            else:
                bass_bar.append(NO_ACTION)
        return bass_bar

    def compose_pad_bar(self, beats_per_bar):
        pad_bar = []
        for i in range(int(beats_per_bar * QUARTER)):
            if i % FULL == 0:
                octave = np.random.randint(3, 5)
                note = np.random.choice(self.scale) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(20, 40)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=FULL + EIGHTH)
                pad_bar.append(msg)
            else:
                pad_bar.append(NO_ACTION)
        return pad_bar

    def compose_melody_bar(self, beats_per_bar):
        if self.scene == 2:
            melody_time = EIGHTH
        else:
            melody_time = QUARTER
        melody_bar = []
        for i in range(int(beats_per_bar * QUARTER)):
            rand = np.random.rand()
            if i % melody_time == 0 and self.intensity < 0.4:
                # print(intensity)
                octave = np.random.randint(5, 7)
                note = np.random.choice(self.scale) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(80, 100)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=QUARTER)
                melody_bar.append(msg)
            else:
                melody_bar.append(NO_ACTION)
        return melody_bar

    def compose_counter_melody_bar(self, beats_per_bar):
        cur_time = time.time()

        counter_melody_bar = []
        # if self.start_time:
        #     if cur_time - self.start_time < 60:
        #         for i in range(int(beats_per_bar * QUARTER)):
        #             counter_melody_bar.append(NO_ACTION)
        #         return counter_melody_bar
        for i in range(int(beats_per_bar * QUARTER)):
            rand = np.random.rand()
            if i % EIGHTH == 0 and self.intensity < 0.6 and rand/2 > self.intensity:
                octave = np.random.randint(5, 8)
                note = np.random.choice(self.scale) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(20, 50)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=EIGHTH)
                counter_melody_bar.append(msg)
            else:
                counter_melody_bar.append(NO_ACTION)

        return counter_melody_bar

    def compose_choir_bar(self, beats_per_bar):
        choir_bar = []
        for i in range(int(beats_per_bar * QUARTER)):
            rand = np.random.rand()
            if i % (EIGHTH + SIXTEENTH) == 0 and self.intensity > 0.69 and rand / 2 < self.intensity:
                # print(intensity)
                octave = np.random.randint(5, 7)
                note = np.random.choice(self.scale) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(40, 90)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=QUARTER * 2)
                choir_bar.append(msg)
            else:
                choir_bar.append(NO_ACTION)
        return choir_bar

    def compose_bar(self, beats_per_bar):
        # beat_bar, pad_bar, melody_bar, counter_melody_bar = [], [], [], []
        # change scale based on the intensity
        if self.intensity > 0.7:
            self.scale = SCALE_C_MINOR
        elif self.intensity > 0.3:
            self.scale = SCALE_F_MINOR
        else:
            self.scale = SCALE_F_MAJOR
        if self.scene == 1:  # introduction
            beat_bar = self.compose_beat_bar(beats_per_bar)
            bar = [beat_bar]
            instrument_list = [0]
            return bar, instrument_list
        elif self.scene == 2:  # setup only beat
            beat_bar = self.compose_beat_bar(beats_per_bar)
            bar = [beat_bar]
            instrument_list = [0]
            return bar, instrument_list
        elif self.scene == 3:  # hear self
            beat_bar = self.compose_beat_bar(beats_per_bar)
            pad_bar = self.compose_pad_bar(beats_per_bar)
            melody_bar = self.compose_melody_bar(beats_per_bar)
            counter_melody_bar = self.compose_counter_melody_bar(beats_per_bar)
            bass_bar = self.compose_bass_bar(beats_per_bar)
            choir_bar = self.compose_choir_bar(beats_per_bar)
            bar = [beat_bar, pad_bar, melody_bar, counter_melody_bar, bass_bar, choir_bar]
            instrument_list = [0, 1, 2, 3, 4, 5]
            return bar, instrument_list
        elif self.scene == 4:  # second setup
            beat_bar = self.compose_beat_bar(beats_per_bar)
            bar = [beat_bar]
            instrument_list = [0]
            return bar, instrument_list
        elif self.scene == 5:  # hear other
            beat_bar = self.compose_beat_bar(beats_per_bar)
            pad_bar = self.compose_pad_bar(beats_per_bar)
            melody_bar = self.compose_melody_bar(beats_per_bar)
            counter_melody_bar = self.compose_counter_melody_bar(beats_per_bar)
            bass_bar = self.compose_bass_bar(beats_per_bar)
            choir_bar = self.compose_choir_bar(beats_per_bar)
            bar = [beat_bar, pad_bar, melody_bar, counter_melody_bar, bass_bar, choir_bar]
            instrument_list = [0, 1, 2, 3, 4, 5]
            return bar, instrument_list
        elif self.scene == 6:  # outro scene - pause (maybe fade out?)
            pass

    def play_bar(self, bar, beats_per_bar, instrument_list):
        for i in range(int(beats_per_bar * QUARTER)):
            self.turn_notes_off()
            time_per_unit = (MINUTE / self.bpm) * BEAT_TO_UNIT_RATIO
            start_unit_time = time.time()
            for k, instrument in enumerate(bar):
                # change instrument volume and stereo spread
                self.control_track.send(mido.Message('control_change', value=int(self.intensity * 127), channel=0))
                self.control_track.send(
                    mido.Message('control_change', value=int((1 - self.intensity) * 127), channel=3))

                # change sound effect tracks volumes
                if self.scene == 3 or self.scene == 5:
                    self.control_track.send(
                        mido.Message('control_change', value=int(self.intensity * 127), channel=1))
                    self.control_track.send(
                        mido.Message('control_change', value=int((1 - self.intensity) * 127), channel=2))
                j = instrument_list[k]
                if not instrument[i] is NO_ACTION:
                    msg = instrument[i].copy()
                    msg.velocity = int(msg.velocity * self.intensity)
                    cur_time = time.time()
                    self.tracks[j].send(instrument[i])
                    self.cur_notes.append([k, msg.note, msg.velocity, cur_time, msg.time * time_per_unit])
            next_unit_time = start_unit_time + time_per_unit
            time_to_wait = next_unit_time - time.time()
            if time_to_wait < 0:
                print("NOT ENOUGH TIME!")
            else:
                time.sleep(time_to_wait)

    def generate(self, generator_stop):
        while not generator_stop.is_set():
            cur_time = time.time()
            if cur_time - self.start_time > INTRODUCNTION_SCENE_END:
                if not self.started_setup_1:
                    print('started setup 1')
                    self.started_setup_1 = True
                    self.scene = 2
                    rain = np.random.randint(2)  # todo - maybe save this data..
                    if rain:
                        print(f"user {self.user} hears rain setup")
                        self.control_track.send(mido.Message('control_change', value=80, channel=1))
                    else:
                        print(f"user {self.user} hears birds setup")
                        self.control_track.send(mido.Message('control_change', value=80, channel=2))
                if cur_time - self.start_time > SETUP_1_END:
                    if not self.started_hear_self:
                        print('started hear self')

                        self.scene = 3
                        self.started_hear_self = True
                        # self.scale = SCALE_F_PENTATONIC
                    if cur_time - self.start_time > HEAR_SELF_END:
                        if not self.started_setup_2:
                            rain = np.random.randint(2)
                            if rain:
                                print(f"user {self.user} hears rain setup")
                                self.control_track.send(mido.Message('control_change', value=80, channel=1))
                                self.control_track.send(mido.Message('control_change', value=0, channel=2))

                            else:
                                print(f"user {self.user} hears birds setup")
                                self.control_track.send(mido.Message('control_change', value=80, channel=2))
                                self.control_track.send(mido.Message('control_change', value=0, channel=1))

                            print('started setup 2')
                            self.started_setup_2 = True
                            self.scene = 4
                        if cur_time - self.start_time > SETUP_2_END:
                            if not self.started_hear_other:
                                # TODO - turn off effect from setup
                                print('started hear other')
                                self.started_hear_other = True
                                self.scene = 5
                            if cur_time - self.start_time > HEAR_OTHER_END:
                                if not self.finished_all:
                                    print('Outro')
                                    self.finished_all = True
                                    self.scene = 6
                                    self.control_track.send(mido.Message('control_change', value=0, channel=0))
                                    self.control_track.send(mido.Message('control_change', value=0, channel=1))
                                    self.control_track.send(mido.Message('control_change', value=0, channel=2))

                                    # self.scale = SCALE_G_MAJOR

            # if self.scene == 2 or self.scene == 4:
            if self.scene != 6:
                self.beats_per_bar = 3 if self.intensity > 0.5 else 2
                bar_notes_all_intstruments, instrument_list = self.compose_bar(self.beats_per_bar)
                self.play_bar(bar_notes_all_intstruments, self.beats_per_bar, instrument_list)


    def start(self):
        self.start_time = time.time()
        self.scene = 1
        self.generator_stop_event = threading.Event()
        generator_thread = threading.Thread(target=self.generate, args=[self.generator_stop_event])
        generator_thread.daemon = True
        generator_thread.start()
        self.control_track.send(START_MSG)
        self.control_track.send(mido.Message('control_change', value=0, channel=1))
        self.control_track.send(mido.Message('control_change', value=0, channel=2))
        # redo all position parameters
        self.started_setup_1 = False
        self.started_hear_self = False
        self.started_setup_2 = False
        self.started_hear_other = False
        self.finished_all = False

    def stop(self):
        if self.generator_stop_event:
            self.generator_stop_event.set()
        for i in range(2):
            time.sleep(0.5)  # todo problematic..
            self.control_track.send(STOP_MSG)
        self.start_time = None

    def close(self):
        self.stop()
        self.server.shutdown()
        self.server.server_close()
        for port in self.tracks:
            port.close()
        self.control_track.close()

    def toggle_take_over_bpm(self):
        self.take_over_heart = not self.take_over_heart

    def toggle_take_over_alpha(self):
        self.take_over_alpha = not self.take_over_alpha

    def toggle_take_over_beta(self):
        self.take_over_beta = not self.take_over_beta

    def toggle_take_over_gamma(self):
        self.take_over_gamma = not self.take_over_gamma

    def toggle_take_over_delta(self):
        self.take_over_delta = not self.take_over_delta

    def toggle_take_over_intensity(self):
        self.take_over_intensity = not self.take_over_intensity

    def slider_heart(self, heart_rate):
        if self.take_over_heart:
            if heart_rate:
                self.bpm = heart_rate
            # print("heart_slider!")
            # print("Current bpm is: {bpm}".format(bpm=self.bpm))

    def slider_alpha(self, alpha_rate):
        if self.take_over_alpha:
            self.alpha = alpha_rate
        # print("Current alpha is: {alpha}".format(alpha=alpha))

    def slider_beta(self, beta_rate):
        if self.take_over_beta:
            self.beta = beta_rate
        # print("Current beta is: {beta}".format(beta=beta))

    def slider_gamma(self, gamma_rate):
        if self.take_over_gamma:
            self.gamma = gamma_rate
        # print("Current gamma is: {gamma}".format(gamma=gamma))

    def slider_delta(self, delta_rate):
        if self.take_over_delta:
            self.delta = delta_rate
        # print("Current delta is: {delta}".format(delta=delta))

    def slider_intensity(self, intensity):
        if self.take_over_intensity:
            # print("setting intensity from SLIDER")
            self.intensity = intensity

    def set_bpm(self, unused_addr, heart_rate):
        if not self.take_over_heart:
            if heart_rate:
                self.bpm = heart_rate

    def set_alpha(self, unused_addr, alpha_rate):
        if not self.take_over_alpha:
            self.alpha = alpha_rate
        # print("Current alpha is: {alpha}".format(alpha=alpha))

    def set_beta(self, unused_addr, beta_rate):
        if not self.take_over_beta:
            self.beta = beta_rate
        # print("Current beta is: {beta}".format(beta=beta))

    def set_gamma(self, unused_addr, gamma_rate):
        if not self.take_over_gamma:
            self.gamma = gamma_rate
        # print("Current gamma is: {gamma}".format(gamma=gamma))

    def set_delta(self, unused_addr, delta_rate):
        if not self.take_over_delta:
            self.delta = delta_rate
        # print("Current delta is: {delta}".format(delta=delta))

    def set_intensity(self, unused_addr, intensity):
        if not self.take_over_intensity:
            # print("setting intensity from osc")
            self.intensity = intensity
        # print("Current delta is: {delta}".format(delta=delta))

    def blink(self, *args):
        # todo do soemthing cool with the music.
        print("#" * 20, "blinking")

    def msg_for_mapping(self, msg=mido.Message('note_on', note=2, channel=0)):
        msg = mido.Message('control_change', value=100)
        self.control_track.send(msg)
        while True:
            note = np.random.randint(40, 127)
            velocity = np.random.randint(0, 127)
            # msg = mido.Message('note_on', note=note, velocity=velocity, time=6.2)
            print(note)
            msg = mido.Message('control_change', value=note, channel=1)
            self.control_track.send(msg)
            time.sleep(0.5)


if __name__ == '__main__':
    ## conttrol traks: channel 0: volume for instruments
    ## channel 1: volume for first sound effect (rain)
    ## channel 2: volume for second sound effect (birds)
    ## channel 3: spatial spread for instruments
    generator = MusicGenerator(user=2)
    note = 60
    velocity = np.random.randint(0, 127)
    msg = mido.Message('control_change', channel=1, control=1, value=122)

    generator.msg_for_mapping(msg)


# # Example how to control some knob. just make sure to map the midi controller on ableton first.
# midi_Cable_1 = mido.open_output('01. Internal MIDI 1')
# msg = mido.Message('control_change', value=100)
# midi_Cable_1.send(msg)
# while True:
#     note = np.random.randint(40, 127)
#     velocity = np.random.randint(0, 127)
#     # msg = mido.Message('note_on', note=note, velocity=velocity, time=6.2)
#     print(note)
#     msg = mido.Message('control_change', value=note)
#
#     midi_Cable_1.send(msg)
#     time.sleep(0.5)
