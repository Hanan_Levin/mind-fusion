import time
import serial
import struct
import serial.tools.list_ports
import numpy as np
from constants import  INTRODUCNTION_SCENE_END, LIGHTS_ON_END, LIGHTS_ON_START, SETUP_2_END, HEAR_OTHER_END, SETUP_1_END
from collections import deque
from sync_plotter import live_plotter
import pickle

BAUD = 9600
NO_ARDUINO_TEXT = '000'

SYNC_WINDOW_LENGTH = 40 * 60


class ArduinoComm():
    def __init__(self, com):
        self.com = com
        self.is_connected = True if com and com != NO_ARDUINO_TEXT else False
        self.baud = BAUD
        self.ser = None
        self.baseline_alpha_1_window = []
        self.baseline_alpha_2_window = []
        self.baseline_heart_1_window = []
        self.baseline_heart_2_window = []
        self.baseline_alpha_1 = None
        self.baseline_alpha_2 = None
        self.baseline_heart_1 = None
        self.baseline_heart_2 = None
        self.sync_window = deque(maxlen=SYNC_WINDOW_LENGTH)
        self.sync_plot_line = []
        self.avg_alpha_diff = None
        self.std_alph_diff = None
        self.sync_data = []  # a list containing all the sync data of the experience (from the part where they hear each other)

        self.fade_in_light = 10
        self.in_plot_thread = []

        # self.find_arduino_port()
        if self.is_connected:
            try:
                self.ser = serial.Serial(self.com, self.baud)
            except Exception as e:
                raise

    def manage_data(self, alpha_1, alpha_2, heart_1, heart_2, start_time):
        cur_time = time.time()
        if cur_time - start_time < SETUP_1_END:  # get baseline measurements
            self.baseline_alpha_1_window.append(alpha_1)
            self.baseline_alpha_1 = np.average(self.baseline_alpha_1_window)
            self.baseline_alpha_2_window.append(alpha_2)
            self.baseline_alpha_2 = np.average(self.baseline_alpha_2_window)
            if heart_1 and heart_2:
                self.baseline_heart_1_window.append(heart_1)
                self.baseline_heart_1 = np.average(self.baseline_heart_1_window)
                self.baseline_heart_2_window.append(heart_2)
                self.baseline_heart_2 = np.average(self.baseline_heart_2_window)
            # turn on the lights during the spoken introduction

        else:
            if LIGHTS_ON_START < cur_time - start_time < LIGHTS_ON_END:
                self.send_int_data(self.fade_in_light)
                self.fade_in_light = min(self.fade_in_light + 2, 254)
            if LIGHTS_ON_END < cur_time - start_time < LIGHTS_ON_END + 3:
                self.fade_in_light = max(self.fade_in_light - 10, 0)
                self.send_int_data(self.fade_in_light)
            alpha_diff = np.absolute(alpha_1 - alpha_2)  # todo! talk to Carmel.
            if heart_1 and heart_2:
                heart_diff = np.absolute(heart_1 - heart_2)  # todo! talk to Carmel.

            if self.avg_alpha_diff and self.std_alph_diff:
                average = self.avg_alpha_diff
                std = self.avg_alpha_diff + self.std_alph_diff
            else:
                average = np.average(np.array(self.sync_window))
                std = np.std(np.array(self.sync_window))

            threshold_1 = average
            threshold_2 = average - std

            # print(alpha_diff)
            if SETUP_2_END < cur_time - start_time < HEAR_OTHER_END:   # send data only when you hear the other
                self.sync_data.append(alpha_diff)
                if alpha_diff < threshold_2:  # todo this is only alpha for now, and no 3 stages..
                    self.send_int_data(254) # todo remember you changed this.

                elif alpha_diff < threshold_1:  # todo this is only alpha for now, and no 3 stages..
                    self.send_int_data(1)
                else:
                    self.send_int_data(0)

            # add the latest sync to the window and plot
            self.sync_window.append(alpha_diff)
            # print("len sync", len(self.sync_window))
            if len(self.sync_window) == SYNC_WINDOW_LENGTH:
                # print("SYNC_WINDOW_START")
                array = np.array(self.sync_window)
                self.avg_alpha_diff = np.average(array)
                self.std_alph_diff = np.std(array)
                self.sync_window = deque(maxlen=SYNC_WINDOW_LENGTH)


    def close(self):
        self.ser.close()
        # todo - check
        if self.sync_data:
            with open('sync_data.pkl', 'wb') as file:
                pickle.dump(self.sync_data, file, protocol=pickle.HIGHEST_PROTOCOL)

    def send_int_data(self, int_data):
        # print("sending data", int_data)
        self.ser.write(struct.pack('>B', int_data))

    def simple_blink_comm_test(self):
        light = 0
        while True:
            light += 100
            light %= 200
            self.ser.write(struct.pack('>B', light))
            print("sending ", light)
            time.sleep(0.5)

    def test_0_1_2_power_lights(self):

        light = 0
        while True:
            light %= 255
            self.ser.write(struct.pack('>B', light))
            print("sending ", light)

            if light in [0,1,2]:
                time.sleep(4)
            else:
                pass
            light += 1



def find_arduino_port():  # Finds which port the arduino is plugged into
    ports = list(serial.tools.list_ports.comports())
    for port in ports:
        if 'USB' in port.description:
            port = port.device
            print(port)


if __name__ == '__main__':
    # port = find_arduino_port()
    arduino_comm = ArduinoComm('COM14')
    arduino_comm.test_0_1_2_power_lights()
