# Mind Fusion - a technological art project
*A team project made together with Carmel Auerbach Ash(Computational Neuroscience PhD student) and Daniella Slonim(BA art student) during the course 'Spectrum - Science, Art, Technology' at the Hebrew University.* 

<img src="/images/mirror.jpeg"  width="450" height="500">

## What is it all about?
Mind Fusion is an interactive art project made to sonify and visualize "hidden" biological data to enrich the experience of interpersonal communication.

## How is the goal achieved?
*	Sit two participants in a secluded intimate environment facing each other.
*	Set each participant with an EEG and heart rate trackers. 
*	Sonify each others biological data based on the psychoacoustic insights.
*	Visualize the synchronicity of the biological data using a mirror like glass which gets more translucent as the biological data is more synchronized, revealing the other participant.

## Technical aspects:
*   [Muse 2](https://choosemuse.com/muse-2/) devices were used to get both EEG and heart rate signals. 
*	Data is received and sent using the OSC protocol.
*	Biological data is filtered and windowed for added robustness.
*	Music is composed in real time using a rule based A.I system crafted for the experience.
*	Musical instructions are sent in Midi form to be synthesized in Ableton Live.
*	Changing the translucence of the glass is achieved by controlling small LED lights placed on the sides of the mirror. As the brightness of the lights changes the glass reflects/reveals different areas allowing for controlling if the glass is see through or acts as a mirror.
*	The lights are controlled through an Arduino, which receives real time commands from the python code running the experience.
*	A visual user interface for running the project was created with the python library kivy.

![](/images/interface.jpg)


## Has it been exhibited?
Apart from being presented in the classroom, the project has also been made accessible to public at Beit Avichai, Jerusalem, at the ['מאין data' exhibition (2019)](https://www.bac.org.il/sdrvt/sdarot/mayn-data).
The below picture was taken at the Beit Avichai exhibition, where an older version of the project was used in which we used a sphere of light that changed its brightness to indicate biological data synchronicity instead of a mirror.

<img src="/images/Beit_Avichai.JPG"  width="600" height="400">





