import time
import rtmidi
import mido
import numpy as np
import threading
from pythonosc import dispatcher
from pythonosc import osc_server
from constants import INTRODUCNTION_SCENE_TIME as SCENE_1_TIME

SELF_IP = "127.0.0.1"
IN_PORT = 9000

MINUTE = 60

SCALE = [0, 2, 3, 5, 7, 8, 10]  # c minor
OCTAVE_INTERVAL = 12
BAR_UNIT = 1 / 16  # todo - maybe this value should change during run time
NO_ACTION = "NA"
SIXTEENTH = 1
EIGHTH = 2
QUARTER = 4
HALF = 8
FULL = 16
BEAT_TO_UNIT_RATIO = 1 / QUARTER

BEAT_MSG = mido.Message('note_on', note=40, velocity=120, time=QUARTER)
BEAT_OFF_MSG = mido.Message('note_off', note=40, velocity=120)

START_MSG = mido.Message('note_on', note=1, channel=0)
STOP_MSG = mido.Message('note_on', note=2, channel=0)

SCENE_1_CONTROL_CHANNEL = 15
SCENE_2_CONTROL_CHANNEL = 14


class MusicGenerator():
    def __init__(self):
        self.set_tracks()
        self.tracks = [self.beat_track, self.pad_track, self.melody_track, self.counter_melody_track]
        self.bpm, self.alpha, self.beta, self.gamma, self.delta, self.intensity = 85, 0.4, 0.5, 0.5, 0.5, 0.5
        self.seconds_per_beat = MINUTE / self.bpm
        self.beats_per_bar = 4
        self.start_time = None
        self.cur_notes = []
        self.set_dispatcher()
        self.scene = None
        self.generator_stop_event = None
        # run the server on a different thread
        self.listen()

        self.take_over_alpha = False
        self.take_over_beta = False
        self.take_over_gamma = False
        self.take_over_delta = False
        self.take_over_heart = False
        self.take_over_intensity = False

    def listen(self):
        self.server = osc_server.ThreadingOSCUDPServer((SELF_IP, IN_PORT), self.dispatcher)
        server_thread = threading.Thread(target=self.server.serve_forever)
        server_thread.daemon = True
        server_thread.start()

    def generator(self, generator_stop):
        while not generator_stop.is_set():
            self.beats_per_bar = 4
            bar_notes_all_intstruments = self.compose_bar(self.beats_per_bar)
            self.play_bar(bar_notes_all_intstruments, self.beats_per_bar)

    def start(self):
        self.start_time = time.time()
        self.scene = 1
        self.generator_stop_event = threading.Event()
        generator_thread = threading.Thread(target=self.generator, args=[self.generator_stop_event])
        generator_thread.daemon = True
        generator_thread.start()
        self.control_track.send(START_MSG)

    def stop(self):
        if self.generator_stop_event:
            self.generator_stop_event.set()
        for i in range(2):
            self.control_track.send(STOP_MSG)

    def close(self):
        self.stop()
        self.server.shutdown()
        self.server.server_close()
        for port in self.tracks:
            port.close()
        self.control_track.close()

    def set_dispatcher(self):
        # Set up an osc server to receive the processed data from the muse.
        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map("/heart_rate", self.set_bpm)
        self.dispatcher.map("/alpha", self.set_alpha)
        self.dispatcher.map("/beta", self.set_beta)
        self.dispatcher.map("/gamma", self.set_gamma)
        self.dispatcher.map("/delta", self.set_delta)
        self.dispatcher.map("/intensity", self.set_intensity)

        self.dispatcher.map("/blink", self.blink)  # todo? tried it out on the muselab but seemed un reliable.

    def set_tracks(self):
        # available_ports = mido.get_output_names()
        self.control_track = mido.open_output('01. Internal MIDI 1', autoreset=True)
        self.beat_track = mido.open_output('02. Internal MIDI 2', autoreset=True)
        self.pad_track = mido.open_output('03. Internal MIDI 3', autoreset=True)
        self.melody_track = mido.open_output('04. Internal MIDI 4', autoreset=True)
        self.counter_melody_track = mido.open_output('05. Internal MIDI 5', autoreset=True)

    def turn_notes_off(self):
        cur_time = time.time()
        removed_notes = []
        for i, note in enumerate(self.cur_notes):
            track, note, velocity, start, length = note
            if cur_time - start > length:
                msg = mido.Message('note_off', note=note, velocity=velocity)
                self.tracks[track].send(msg)
                removed_notes.append(i)
        for i in removed_notes[::-1]:
            self.cur_notes.remove(self.cur_notes[i])

    def compose_beat_bar(self, beats_per_bar):
        beat_bar = []
        for i in range(int(beats_per_bar * 1 / BAR_UNIT)):
            if i % QUARTER == 0:
                beat_bar.append(BEAT_MSG)
            else:
                beat_bar.append(NO_ACTION)
        return beat_bar

    def compose_pad_bar(self, beats_per_bar):
        pad_bar = []
        for i in range(int(beats_per_bar * 1 / BAR_UNIT)):
            if i % FULL == 0:
                octave = np.random.randint(3, 5)
                note = np.random.choice(SCALE) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(20, 40)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=FULL + EIGHTH)
                pad_bar.append(msg)
            else:
                pad_bar.append(NO_ACTION)
        return pad_bar

    def compose_melody_bar(self, beats_per_bar):
        melody_bar = []
        for i in range(int(beats_per_bar * 1 / BAR_UNIT)):
            rand = np.random.rand()
            if i % QUARTER == 0 and rand < self.intensity:
                # print(intensity)
                octave = np.random.randint(4, 5)
                note = np.random.choice(SCALE) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(80, 100)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=QUARTER)
                melody_bar.append(msg)
            else:
                melody_bar.append(NO_ACTION)
        return melody_bar

    def play_counter_melody(self, last_counter_melody_time):
        cur_time = time.time()
        if cur_time - last_counter_melody_time >= 0.5 * self.seconds_per_beat and cur_time - self.start_time > 16 * self.seconds_per_beat:
            octave = np.random.randint(5, 8)
            note = np.random.choice(SCALE) + OCTAVE_INTERVAL * octave
            velocity = np.random.randint(20, 50)
            msg = mido.Message('note_on', note=note, velocity=velocity, channel=1)
            self.counter_melody_track.send(msg)
            last_counter_melody_time = time.time()

            self.cur_notes.append([3, note, velocity, cur_time, 0.5])
        return last_counter_melody_time

    def compose_counter_melody_bar(self, beats_per_bar):
        counter_melody_bar = []
        for i in range(int(beats_per_bar * 1 / BAR_UNIT)):
            rand = np.random.rand()
            if i % EIGHTH == 0 and rand < self.intensity:
                octave = np.random.randint(5, 8)
                note = np.random.choice(SCALE) + OCTAVE_INTERVAL * octave
                velocity = np.random.randint(20, 50)
                msg = mido.Message('note_on', note=note, velocity=velocity, time=EIGHTH)
                counter_melody_bar.append(msg)
            else:
                counter_melody_bar.append(NO_ACTION)
        return counter_melody_bar

    def compose_bar(self, beats_per_bar):
        # beat_bar, pad_bar, melody_bar, counter_melody_bar = [], [], [], []
        beat_bar = self.compose_beat_bar(beats_per_bar)
        pad_bar = self.compose_pad_bar(beats_per_bar)
        melody_bar = self.compose_melody_bar(beats_per_bar)
        counter_melody_bar = self.compose_counter_melody_bar(beats_per_bar)

        bar = [beat_bar, pad_bar, melody_bar, counter_melody_bar]
        return bar

    def play_bar(self, bar, beats_per_bar):
        time_per_unit = (MINUTE / self.bpm) * BEAT_TO_UNIT_RATIO
        for i in range(int(beats_per_bar * QUARTER)):
            self.turn_notes_off()
            start_unit_time = time.time()
            for j, instrument in enumerate(bar):
                if not instrument[i] is NO_ACTION:
                    msg = instrument[i]
                    msg.velocity = int(msg.velocity * self.intensity)
                    cur_time = time.time()
                    self.tracks[j].send(instrument[i])
                    self.cur_notes.append([j, msg.note, msg.velocity, cur_time, msg.time * time_per_unit])
            next_unit_time = start_unit_time + time_per_unit
            time_to_wait = next_unit_time - time.time()
            if time_to_wait < 0:
                print("NOT ENOUGH TIME!")
            else:
                time.sleep(time_to_wait)

    def toggle_take_over_bpm(self):
        self.take_over_heart = not self.take_over_heart

    def toggle_take_over_alpha(self):
        self.take_over_alpha = not self.take_over_alpha

    def toggle_take_over_beta(self):
        self.take_over_beta = not self.take_over_beta

    def toggle_take_over_gamma(self):
        self.take_over_gamma = not self.take_over_gamma

    def toggle_take_over_delta(self):
        self.take_over_delta = not self.take_over_delta

    def toggle_take_over_intensity(self):
        self.take_over_intensity = not self.take_over_intensity

    def slider_heart(self, heart_rate):
        if self.take_over_heart:
            self.bpm = heart_rate
            # print("heart_slider!")
            # print("Current bpm is: {bpm}".format(bpm=self.bpm))

    def slider_alpha(self, alpha_rate):
        if self.take_over_alpha:
            self.alpha = alpha_rate
        # print("Current alpha is: {alpha}".format(alpha=alpha))

    def slider_beta(self, beta_rate):
        if self.take_over_beta:
            self.beta = beta_rate
        # print("Current beta is: {beta}".format(beta=beta))

    def slider_gamma(self, gamma_rate):
        if self.take_over_gamma:
            self.gamma = gamma_rate
        # print("Current gamma is: {gamma}".format(gamma=gamma))

    def slider_delta(self, delta_rate):
        if self.take_over_delta:
            self.delta = delta_rate
        # print("Current delta is: {delta}".format(delta=delta))

    def slider_intensity(self, intensity):
        if self.take_over_intensity:
            # print("setting intensity from SLIDER")
            self.intensity = intensity

    def set_bpm(self, unused_addr, heart_rate):
        if not self.take_over_heart:
            self.bpm = heart_rate

    def set_alpha(self, unused_addr, alpha_rate):
        if not self.take_over_alpha:
            self.alpha = alpha_rate
        # print("Current alpha is: {alpha}".format(alpha=alpha))

    def set_beta(self, unused_addr, beta_rate):
        if not self.take_over_beta:
            self.beta = beta_rate
        # print("Current beta is: {beta}".format(beta=beta))

    def set_gamma(self, unused_addr, gamma_rate):
        if not self.take_over_gamma:
            self.gamma = gamma_rate
        # print("Current gamma is: {gamma}".format(gamma=gamma))

    def set_delta(self, unused_addr, delta_rate):
        if not self.take_over_delta:
            self.delta = delta_rate
        # print("Current delta is: {delta}".format(delta=delta))

    def set_intensity(self, unused_addr, intensity):
        if not self.take_over_intensity:
            # print("setting intensity from osc")
            self.intensity = intensity
        # print("Current delta is: {delta}".format(delta=delta))

    def blink(self, *args):
        # todo do soemthing cool with the music.
        print("#" * 20, "blinking")

    def msg_for_mapping(self):
        msg = mido.Message('note_on', note=2, channel=0)
        self.control_track.send(msg)


if __name__ == '__main__':
    generator = MusicGenerator()
    # generator.msg_for_mapping()
    print("starting")
    generator.start()
    time.sleep(5)
    print("stoping")
    generator.stop()
    time.sleep(5)
    print("starting")
    generator.start()
    time.sleep(5)

# # Example how to control some knob. just make sure to map the midi controller on ableton first.
# midi_Cable_1 = mido.open_output('01. Internal MIDI 1')
# msg = mido.Message('control_change', value=100)
# midi_Cable_1.send(msg)
# while True:
#     note = np.random.randint(40, 127)
#     velocity = np.random.randint(0, 127)
#     # msg = mido.Message('note_on', note=note, velocity=velocity, time=6.2)
#     print(note)
#     msg = mido.Message('control_change', value=note)
#
#     midi_Cable_1.send(msg)
#     time.sleep(0.5)
